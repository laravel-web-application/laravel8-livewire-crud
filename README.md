<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel 8 Livewire CRUD
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel8-livewire-crud.git`
2. Go inside the folder: `cd laravel8-livewire-crud`
3. Run `cp .env.example .env` then put your database name, credentials & AWS Key.
4. Run `composer install`
5. Run `php artisan key:generate`
6. Run `php artisan migrate`
7. Run `php artisan serve`
8. Open your favorite browser: http://localhost:8000

### Screen shot

Register Page

![Register Page](img/register.png "Register Page")

Login Page

![Login Page](img/login.png "Login Page")

Home Page

![Home Page](img/home.png "Home Page")

Dashboard Page

![Dashboard Page](img/dashboard.png "Dashboard Page")

Add New Post

![Add New Post](img/add.png "Add New Post")

List Post Page

![List Post Page](img/post1.png "List Post Page")

![List Post Page](img/post2.png "List Post Page")
